package lapcsere;

public class Keret {

	private char betu;
	private int szam;
	private int fagyas = 0;

	public Keret(char betu, int szam) {
		this.betu = betu;
		this.szam = szam;
	}

	public boolean hasznal(int szam) {
		if (this.szam != szam) {
			return false;
		}
		this.fagyas = 0;
		System.out.print('-');
		return true;
	}

	public boolean belerakHaTud(int szam) {
		if (this.fagyas > 0) {
			return false;
		}
		this.szam = szam;
		this.fagyas = 3+1;
		System.out.print(this.betu);
		return true;
	}

	public void telikAzIdo() {
		if (this.fagyas > 0) {
			--this.fagyas;
		}
	}

}
