package lapcsere;

import java.util.ArrayList;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class Main {

	public static int[] beolvas() {
		Scanner olvaso = new Scanner(System.in);
		String sor = null;
		try {
			sor = olvaso.nextLine();
		} catch (NoSuchElementException noSuchElementException) {
			return null;
		} finally {
			olvaso.close();
		}
		String[] darabolt = sor.split(",");
		int[] bemenet = new int[darabolt.length];
		for (int i = 0; i < darabolt.length; ++i) {
			bemenet[i] = Math.abs(Integer.parseInt(darabolt[i]));
		}
		return bemenet;
	}

	public static void main(String[] args) {
		int[] bemenet = beolvas();
		int memoriahibakSzama = 0;
		if (bemenet != null) {
			ArrayList<Keret> keretek = new ArrayList<Keret>();
			keretek.add(new Keret('A', 0));
			keretek.add(new Keret('B', 0));
			keretek.add(new Keret('C', 0));
			KULSO: for (int szam : bemenet) {
				for (Keret keret : keretek) {
					keret.telikAzIdo();
				}
				for (int i = 0; i < 3; ++i) {
					if (keretek.get(i).hasznal(szam)) {
						keretek.add(keretek.remove(i));
						continue KULSO;
					}
				}
				++memoriahibakSzama;
				for (int i = 0; i < 3; ++i) {
					if (keretek.get(i).belerakHaTud(szam)) {
						keretek.add(keretek.remove(i));
						continue KULSO;
					}
				}
				System.out.print('*');
			}
		}
		System.out.println();
		System.out.println(memoriahibakSzama);
	}

}
